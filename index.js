document.onkeydown = updateKey;
document.onkeyup = resetKey;

var server_port = 65432;
var server_addr = "192.168.1.129";   // the IP address of your Raspberry PI

function client(){
    
    const net = require('net');
    var input = document.getElementById("message").value;

    const client = net.createConnection({ port: server_port, host: server_addr }, () => {
        // 'connect' listener.
        console.log('connected to server!');
        // send the message
        client.write(`${input}`);
    });
    
    // get the data from the server
    client.on('data', (data) => {
        const response = data.toString();

        const [disPart, directionPart, powerPart, tempPart, objDisPart, objPart] = response.split(', ');

        // Extract values
        const disValue = disPart.split(': ')[1].trim();
        const directionValue = directionPart.split(': ')[1].trim();
        const powerValue = powerPart.split(': ')[1].trim();
        const tempValue = tempPart.split(': ')[1].trim();
        const objDisValue = objDisPart.split(': ')[1].trim();
        const objValue = objPart.trim();

        // Update Bluetooth return value
        document.getElementById("bluetooth").innerHTML = `DIS: ${disValue}`;

        // Update Car Direction
        document.getElementById("direction").innerHTML = `${directionValue}`;

        // Update Distance Traveled
        document.getElementById("distance").innerHTML = `${disValue}`;

        // Update Speed/Power
        document.getElementById("speed").innerHTML = `${powerValue}`;

        // Update Temperature
        document.getElementById("temperature").innerHTML = `${tempValue}`;

        // Update Object status
        document.getElementById("obj").innerHTML = `${objValue}`;

        // Update Object Distance
        document.getElementById("obj_dis").innerHTML = `${objDisValue}`;

        console.log(data.toString());
        client.end();
        client.destroy();
    });

    client.on('end', () => {
        console.log('disconnected from server');
    });


}

// for detecting which key is been pressed w,a,s,d
function updateKey(e) {

    e = e || window.event;

    if (e.keyCode == '87') {
        // up (w)
        document.getElementById("upArrow").style.color = "green";
        send_data("up");
    }
    else if (e.keyCode == '83') {
        // down (s)
        document.getElementById("downArrow").style.color = "green";
        send_data("back");
    }
    else if (e.keyCode == '65') {
        // left (a)
        document.getElementById("leftArrow").style.color = "green";
        send_data("left");
    }
    else if (e.keyCode == '68') {
        // right (d)
        document.getElementById("rightArrow").style.color = "green";
        send_data("right");
    }
    else if (e.keyCode == '81') {
        // 'e' key
        document.getElementById("qKey").style.color = "green";
        send_data("stop");
    }
}

// reset the key to the start state 
function resetKey(e) {

    e = e || window.event;

    document.getElementById("upArrow").style.color = "grey";
    document.getElementById("downArrow").style.color = "grey";
    document.getElementById("leftArrow").style.color = "grey";
    document.getElementById("rightArrow").style.color = "grey";
}


// update data for every 50ms
function update_data(){
    setInterval(function(){
        // get image from python server
        client();
    }, 200);
}
function send_data(direction) {
    var input = direction;
    const net = require('net');

    const client = net.createConnection({ port: server_port, host: server_addr }, () => {
        // 'connect' listener.
        console.log('connected to server!');
        // send the message
        client.write(`${input}`);
    });

}