import picar_4wd as fc
import time
from gpiozero import CPUTemperature
##NOTE: this file must put inside of the foler under picar_4wd to use. better under examples


HOST = "192.168.1.129" # IP address of your Raspberry PI
PORT = 65432          # Port to listen on (non-privileged ports are > 1023)


power = 20
#dis = 0
#direction = 0


def callcar(data, dis, direction, obj_dis):
    s = data.upper()
    trig = fc.Pin("D8")
    echo = fc.Pin("D9")
    t = fc.Ultrasonic(trig,echo)
    obj_dis = t.get_distance()
    #car move forward using "up"
    if "UP" in s:
        fc.forward(power)
        dis += 1
    if "BACK" in s:
        fc.backward(power)
        dis -= 1
    if "LEFT" in s:
        fc.turn_left(power)
        dis += 0.3
        direction -= 10
    if "RIGHT" in s:
        fc.turn_right(power)
        dis += 0.3
        direction += 10
    if "STOP" in s:
        fc.stop()

    return dis, direction, obj_dis


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    dis = 0
    direction = 0
    obj_dis = 999
    try:
        while 1:
            client, clientInfo = s.accept()
            print("server recv from: ", clientInfo)
            data = client.recv(1024)      # receive 1024 Bytes of message in binary format
            if data != b"":
                #print(f'dis:{dis}')
                #print(data)
                #exit()
                if data.decode('utf-8').upper() in ['UP','BACK','LEFT','RIGHT','STOP']:

                    dis, direction, obj_dis = callcar(data.decode('utf-8'), dis, direction, obj_dis)
                    print( data.decode('utf-8'))
                if data.decode('utf-8').upper() in ['UPDATE']:
                    cpu = CPUTemperature()
                    obj = False
                    if obj_dis < 15:
                        obj = True
                    if obj_dis >= 15:
                        obj = False
                    sign = False
                    if direction < 0:
                        sign = True
                    temp = abs(direction)
                    if sign:
                        dir = 'left'
                    if not sign:
                        dir = 'right'
                    response = f"DIS: {dis}, DIRECTION: {dir}, POWER: {power}, TEMP: {cpu.temperature}, OBJ_DIS: {obj_dis}, OBJ: {obj}"
                    print(response)
                    client.sendall(response.encode('utf-8')) # Echo back to client
                #callcar(data)
    except Exception as e:
        print("Closing socket with err, ", e)
        client.close()
        s.close()
        exit()